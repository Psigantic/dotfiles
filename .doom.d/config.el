;; Look and feel
(setq doom-font (font-spec :family "DejaVu Sans Mono" :size 15)
      doom-big-font (font-spec :family "Dejavu Sans Mono"  :size 23)
      doom-theme 'doom-molokai
      display-line-numbers-type 'relative
      confirm-kill-emacs nil
      doom-modeline-major-mode-icon t
      fancy-splash-image "~/.doom.d/Psigantic_logo.svg"
      doom-modeline-height 1)
;;;; keep modeline font small
(add-hook! 'doom-big-font-mode-hook
        (set-face-attribute 'mode-line nil :height 100)
        (set-face-attribute 'mode-line-inactive nil :height 100))
;;;; Start maximized
(add-to-list 'initial-frame-alist '(fullscreen . maximized))

;; Languages
;;;; General
;;;;;; lsp always show breadcrumb
(setq lsp-headerline-breadcrumb-enable t)

;;;; Kotlin
;; Enable java-imports
(require 'java-imports)
(add-hook 'java-mode-hook 'java-imports-scan-file)
(add-hook 'kotlin-mode-hook 'java-imports-scan-file)

;; Keybinds
;;;; rgrep in project
(map! :leader
      (:prefix-map ("s" . "search")
       :desc "rgrep in project" "r" #'rgrep))

;;;; Ctrl+vim navigation keys in the evil edit mode
(map! :i "C-l" #'forward-char
      :i "C-h" #'backward-char
      :i "C-k" #'previous-line
      :i "C-j" #'next-line
      :i "C-p" #'previous-line
      :i "C-n" #'next-line)

;;;; jump to paranthesis with tab
(map! :n [tab] 'evil-jump-item)

;;;; expand region
(map! :leader
      (:desc "Expand region"  "v" #'er/expand-region))

;;;; rename buffer
(map! :leader
      (:prefix "b"
       :desc "Rename the buffer" "R" #'rename-buffer))

;;;; debugger
(map! :leader
      (:prefix "o"
       :desc "Debugger start last" "l" #'+debugger/start-last
       :desc "Debugger quit" "q" #'+debugger/quit))

;;;; toggle keycast-mode
(map! :leader
      (:prefix "t"
       :desc "keycast" "k" #'keycast-mode))

;;;; winner undo, redo (undo window configuration)
(map! "C-c <left>" 'winner-undo
      "C-c <right>" 'winner-redo)

;;;; drag text up-down
(map! :n "M-p" 'drag-stuff-up
      :n "M-n" 'drag-stuff-down)

;;;; stop compilation
(map! :leader
      (:prefix "c"
       :desc "Kill compilation" "K" #'kill-compilation))

;;;; Dired
;;;;;; hide files toggle on M-h
(setq my-dired-ls-switches "-alh --ignore=.* --ignore=\\#* --ignore=*~")
(setq my-dired-switch 1)
(add-hook 'dired-mode-hook
          (lambda ()
            (dired-hide-details-mode)  ; enable by "("
            (define-key dired-mode-map (kbd "M-h")
              (lambda ()
                "Toggle between hide and show."
                (interactive)
                (setq my-dired-switch (- my-dired-switch))
                (if (= my-dired-switch 1)
                    (dired-sort-other my-dired-ls-switches)
                  (dired-sort-other "-alh"))))))

;;;;;; don't ask questions about size
(setq large-file-warning-threshold nil)

;;;; Misc
;;;;;; projectile project discovery path
(setq projectile-project-search-path '("~/Projects/Code" "~/Projects/Playground"))

;;;;;; Ansi colors in buffer
(defun display-ansi-colors ()
  (interactive)
  (ansi-color-apply-on-region (point-min) (point-max)))

;;;;;; undo-tree everywhere
(setq global-undo-tree-mode t)

;;;;;; keycast with doom modeline
(after! keycast
  (define-minor-mode keycast-mode
    "Show current command and its key binding in the mode line."
    :global t
    (if keycast-mode
        (progn
                (add-hook 'pre-command-hook 'keycast--update t)
                (add-to-list 'global-mode-string '("" mode-line-keycast)))
      (progn
         (remove-hook 'pre-command-hook 'keycast-mode-line-update)
         (setq global-mode-string (delete '("" mode-line-keycast " ") global-mode-string)))))
  (setq keycast-substitute-alist '((evil-next-line nil nil)
                                   (evil-previous-line nil nil)
                                   (evil-forward-char nil nil)
                                   (evil-backward-char nil nil)
                                   (ivy-done nil nil)
                                   (self-insert-command nil nil))))
(add-to-list 'global-mode-string '("" mode-line-keycast))
